
# DevOps - Faculdade Impacta

<p Align="center">
<img src="static/Assets/img/logo-universidade-impacta.png" width="100%" height="50%">
</p>

Neste trabalho estamos gerando um app utilizando Python e Flask, também estamos subindo uma imagem docker contendo nosso projeto.

<BR>
<BR>

<p Align="center">
<img src="static/Assets/img/astro-img.png">
</p>


<p Align="center">
<a hrf="https://github.com/Wesley-Alexsander?tab=repositories">Project by Wesley Alexsander</a>
</p>

<br>
<br>

# 🆙 Flask Docker
Modelo para uso de uma aplicação Python + Flask, utilizando Docker.

Build da imagem Docker

```code
$ docker build --tag seu-usuário/my-flask-app-docker .
[+] Building 69.7s (4/9)
=> [1/5] FROM docker.io/library/python@sha256:d3
.
.
.
$ docker images
REPOSITORY TAG IMAGE ID CREATED SIZE
my-flask-app-docker latest 40f8ac1a4476 About a minute ago 941MB
```
# Testando

```code
$ docker run -d -p 5000:5000 seu-usuário/my-flask-app-docker
id-contêiner
$ curl localhost:5000
<h1>Hello World!</h1>
```
# Finalizando
```code
$ docker stop id-contêiner
```

<br>
<br>

# 🎨 **Style Guide**

**Cores:**

```css
:root {
   /*====== Color ======*/
  --hue: 206;
  --black-color: hsl(var(--hue), 4%, 4%);
  --alternative-black: hsl(var(--hue), 4%, 8%);
  --title-color: hsl(var(--hue), 4%, 95%);
  --text-color: hsl(var(--hue), 4%, 75%);
  --text-color-light: hsl(var(--hue), 4%, 65%);
  --body-color: hsl(var(--hue), 4%, 6%);
  --white-color: #fff;
  --container-color: hsl(var(--hue), 4%, 10%);
  --text-gradient: linear-gradient(
    hsl(var(--hue), 4%, 24%),
    hsl(var(--hue), 4%, 8%)
  );
  --scroll-thumb-color: hsl(var(--hue), 4%, 16%);
  --scroll-thumb-color-alt: hsl(var(--hue), 4%, 20%);
}
```


<br>
<br>

## 📜✏️ **Tipo de fonte:**

font-family: Poppins, Sans-serif

font-weight: 500, 600

```css
:root {
      /*========== Font and typography ==========*/
  --body-font: 'Poppins', sans-serif;
  --biggest-font-size: 5rem;
  --bigger-font-size: 3rem;
  --big-font-size: 2.5rem;
  --h2-font-size: 1.25rem;
  --h3-font-size: 1.125rem;
  --normal-font-size: 0.938rem;
  --small-font-size: 0.813rem;
  --smaller-font-size: 0.75rem;
  --text-line-height: 2rem;

    /*========== Font weight ==========*/
  --font-medium: 500;
  --font-semi-bold: 600;
        
}
```



Você pode encontrar a fonte no [Google Fonts](https://fonts.google.com/)